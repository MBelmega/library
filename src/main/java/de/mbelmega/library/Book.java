package de.mbelmega.library;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Book {
   //Seitenzahl, Kapitel, Inhaltsverzeichnis, Titel, Autor, Erscheinungsjahr, Verlag
    @Id
    @GeneratedValue
    private long id;
    private String title;
    private String author;
    private String publisher;
    private int year;

    public Book(String title, String author, int year, String publisher) {
        this.title =title;
        this.author=author;
        this.year=year;
        this.publisher=publisher;
    }

    public Book() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }
}
