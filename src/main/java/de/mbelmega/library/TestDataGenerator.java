package de.mbelmega.library;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class TestDataGenerator {

    @Autowired
    BookRepository repo;

    @EventListener(ApplicationReadyEvent.class)
    public void createTestbooks() {
        Book majasFirstBook = new Book("Majs Wundersame Welt",
                "Willy May", 1985, "Rudis Verlag");
        repo.save(majasFirstBook);

    }

}
